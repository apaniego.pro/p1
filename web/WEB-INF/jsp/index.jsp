<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <p>Hello! This is the default welcome page for a Spring Web MVC project.</p>
        <p><i>To display a different welcome page for this project, modify</i>
            <tt>index.jsp</tt> <i>, or create your own welcome page then change
                the redirection in</i> <tt>redirect.jsp</tt> <i>to point to the new
                welcome page and also update the welcome-file setting in</i>
            <tt>web.xml</tt>.</p>
       
        <form action="S2" method="POST" >
            <p>
                Ingresa tu nombre completo: <input type="text" name="nombrecompleto">
                <select name="selectcategorias">
                    <option selected>SELECCIONE UNA CATEGORIA</option>
                    <option value="1" >CIENCIAS DE LA COMPUTACIÓN</option>
                    <option value="2" >QUÍMICA E INGENIERÍA DEL PETRÓLEO</option>
                    <option value="3" >INGENIERÍA AMBIENTAL</option>
                    <option value="4" >INGENIERÍA MECÁNICA</option>
                    <option value="5" >INGENIERÍA CIVIL</option>
                    <option value="6" >ELECTRÓNICA</option>
                </select><br>
                Select Sports:<br>
                <input type="checkbox" name="sports" value="Cricket">Cricket<br>
                <input type="checkbox" name="sports" value="Football">Football<br>
                <input type="checkbox" name="sports" value="Hockey">Hockey<br>
                <input type="checkbox" name="sports" value="Tenis">Tenis<br>
                <br>
                <input type="radio" name="transporte" value="Coche">Coche<br>
                <input type="radio" name="transporte" value="Avión">Avión<br>
                <input type="radio" name="transporte" value="Tren">Tren<br>
                <br>
                <select name="cars" id="cars" multiple>
                    <option value="volvo">Volvo</option>
                    <option value="saab">Saab</option>
                    <option value="opel">Opel</option>
                    <option value="audi">Audi</option>
                </select>

                <input type="submit" value="Enviar">
            </p>
        </form>

    </body>
</html>
